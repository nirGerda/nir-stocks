mvn spring-boot:run

["AAPL","ABBV","ABT","ACN","AGN","AIG","ALL","AMGN","AMZN","AXP","BA","BAC","BIIB","BK","BKNG","BLK","BMY","BRK.B","C","CAT","CELG","CHTR","CL","CMCSA","COF","COP","COST","CSCO","CVS","CVX","DHR","DIS","DUK","DWDP","EMR","EXC","F","FB","FDX","FOX","FOXA","GD","GE","GILD","GM","GOOG","GOOGL","GS","HAL","HD","HON","IBM","INTC","JNJ","JPM","KHC","KMI","KO","LLY","LMT","LOW","MA","MCD","MDLZ","MDT","MET","MMM","MO","MRK","MS","MSFT","NEE","NFLX","NKE","NVDA","ORCL","OXY","PEP","PFE","PG","PM","PYPL","QCOM","RTN","SBUX","SLB","SO","SPG","T","TGT","TXN","UNH","UNP","UPS","USB","UTX","V","VZ","WBA","WFC","WMT","XOM"]
["ATVI","ADBE","AMD","ALXN","ALGN","GOOGL","GOOG","AMZN","AAL","AMGN","ADI","AAPL","AMAT","ASML","ADSK","ADP","BIDU","BIIB","BMRN","BKNG","AVGO","CDNS","CELG","CERN","CHTR","CHKP","CTAS","CSCO","CTXS","CTSH","CMCSA","COST","CSX","CTRP","DLTR","EBAY","EA","EXPE","FB","FAST","FISV","GILD","HAS","HSIC","IDXX","ILMN","INCY","INTC","INTU","ISRG","JBHT","JD","KLAC","LRCX","LBTYA","LBTYK","LULU","MAR","MXIM","MELI","MCHP","MU","MSFT","MDLZ","MNST","MYL","NTAP","NTES","NFLX","NVDA","NXPI","ORLY","PCAR","PAYX","PYPL","PEP","QCOM","REGN","ROST","SIRI","SWKS","SBUX","SYMC","SNPS","TMUS","TTWO","TSLA","TXN","KHC","FOXA","FOX","ULTA","UAL","VRSN","VRSK","VRTX","WBA","WDC","WLTW","WDAY","WYNN","XEL","XLNX"]


my up stocks 2.8.:
["axp" ,"aapl", "ba", "all","bkng","chtr","lmt","mdlz","mmm","pm","sbux","utx","adi","asml","cdns","chkp","dltr","ebay","expe","lbtya","lbtyk","lrcx","mar","mu","payx","pcar","swks","ual","vrsn","wynn"]
["mchp", "ctas", "spg", "csx", "so","ctsh","emr","axp","ba" ,"all","chtr","sbux","adi","cdns","chkp","expe","payx","swks"]

mystocks 12.2 : (18+14)
["mchp", "ctas", "spg", "csx", "so","ctsh","emr","axp","ba" ,"all","chtr","sbux","adi","cdns","chkp","expe","payx","swks","mmm","ebay","asml","ctxs","lrcx","adsk","jbht","ups","pcar","mdlz","orly","pm","mu","ual","intc","tsla","utx","aapl","wynn","lmt","mrk","dltr"]

23.2 : 30 stocks of the week:
["xlnx","ebay","adi","adsk","nxpi","adp","ba","ctas","cdns","snps"   ,"pm","wday","pcar","ma","payx","chkp","alxn","rtn","mar","mdlz"   ,"wynn","axp","mmm","acn","kmi","txn","aapl","incy","nflx","csx"]

stocks under tracking
["xlnx","cdns","nxpi","wynn","adp","snps"]