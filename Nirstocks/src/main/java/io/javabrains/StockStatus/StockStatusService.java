package io.javabrains.StockStatus;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.Stock.Stock;

@Service
public class StockStatusService {

	@Autowired
	private StockStatusRepositry stockStatusRepositry;

	public List<StockStatus> getStockStatusById(String stockId) {
		List<StockStatus> listStockStatus = new ArrayList<>();
		stockStatusRepositry.findByStockOwnerSymbolId(stockId).forEach(listStockStatus::add);
		return listStockStatus;
	}

	public void addStockStatus(StockStatus newStockStatus) {
		stockStatusRepositry.save(newStockStatus);
	}

	public List<StockStatus> getStockStatus(Stock stock) {

		List<StockStatus> history = new ArrayList<>();
		stockStatusRepositry.findByStockOwner(stock).forEach(history::add);
		return history;
	}

	public void deleteByStockOwner(Stock s) {
		stockStatusRepositry.deleteByStockOwner(s);
	}

	public void addLisStockstStatus(List<StockStatus> stockStatus) {

		stockStatusRepositry.save(stockStatus);

	}

	public void deleteAll() {
		stockStatusRepositry.deleteAll();

	}

	@Transactional
	public void deleteByStockSymbol(String symbolId) {
		stockStatusRepositry.deleteByStockOwnerSymbolId(symbolId);

	}
}
