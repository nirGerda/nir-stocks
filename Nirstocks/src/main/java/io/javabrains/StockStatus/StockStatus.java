package io.javabrains.StockStatus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.javabrains.Stock.Stock;

@Entity
public class StockStatus {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private Date date;
	private String dateStr;
	private double opens;
	private double high;
	private double low;
	private double closes;
	private double changes;
	private double changePercent;
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "stock_id")
	@JsonBackReference
	private Stock stockOwner;

	public StockStatus() {
	}

	public StockStatus(StockStatusConverter convertFrom) {
		this.opens = convertFrom.getOpen();
		this.high = convertFrom.getHigh();
		this.low = convertFrom.getLow();
		this.closes = convertFrom.getClose();
		this.changes = convertFrom.getChange();
		this.changePercent = convertFrom.getChangePercent();
	}

	public StockStatus(JSONObject stockData) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.date = formatter.parse(stockData.getString("date"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.dateStr = stockData.getString("date");
		this.opens = stockData.getDouble("open");
		this.high = stockData.getDouble("high");
		this.low = stockData.getDouble("low");
		this.closes = stockData.getDouble("close");
		this.changes = stockData.getDouble("change");
		this.changePercent = stockData.getDouble("changePercent");

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Stock getStockOwner() {
		return stockOwner;
	}

	public void setStockOwner(Stock stockOwner) {
		this.stockOwner = stockOwner;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(String date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.date = formatter.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public double getOpens() {
		return opens;
	}

	public void setOpens(double opens) {
		this.opens = opens;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getCloses() {
		return closes;
	}

	public void setCloses(double closes) {
		this.closes = closes;
	}

	public double getChanges() {
		return changes;
	}

	public void setChange(double change) {
		this.changes = change;
	}

	public double getChangePercent() {
		return changePercent;
	}

	public void setChangePercent(double changePercent) {
		this.changePercent = changePercent;
	}

	public String getDateStr() {
		return dateStr;
	}

	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}

//	String inputDate = "07/28/2011 11:06:37 AM";
//	Date date = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a").parse(inputDate);
//	String outputDate = new SimpleDateFormat("MMM-dd-yyyy HH:mm:ss").format(date);
//	System.out.println(outputDate); // Jul-28-2011 11:06:37

}