package io.javabrains.StockStatus;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.javabrains.Stock.Stock;

public interface StockStatusRepositry extends CrudRepository<StockStatus, Long> {
	public List<StockStatus> findByStockOwnerSymbolId(String stockId);

	public List<StockStatus> findByStockOwner(Stock stock);

	public void deleteByStockOwner(Stock stockOwner);

	public void deleteByStockOwnerSymbolId(String symbolId);

}
