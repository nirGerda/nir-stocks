package io.javabrains.StockQuote;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.javabrains.Stock.Stock;

@Entity
public class StockQuote {
	@Id
	private String symbolId;
	private String companyName;
	private String primaryExchange;
	private double opens;/* problem name open */
	private double high;
	private double low;
	private double latestPrice;
	private double closes;/* problem name close */
	private double changes;
	private double changePercent;
	private double ytdChange;
	@OneToOne
	@JoinColumn(name = "stock_id")
	@JsonBackReference
	private Stock stockOwner;

	public StockQuote() {
	}

	public StockQuote(StockQuoteConverter convertFrom) {
		this.symbolId = convertFrom.getSymbol();
		this.companyName = convertFrom.getCompanyName();
		this.primaryExchange = convertFrom.getPrimaryExchange();
		this.opens = convertFrom.getOpen();
		this.high = convertFrom.getHigh();
		this.low = convertFrom.getLow();
		this.latestPrice = convertFrom.getLatestPrice();
		this.closes = convertFrom.getClose();
		this.changes = convertFrom.getChange();
		this.changePercent = convertFrom.getChangePercent();
		this.ytdChange = convertFrom.getYtdChange();
	}

	public StockQuote(JSONObject stockData) {
		this.symbolId = stockData.getString("symbol");
		this.companyName = stockData.getString("companyName");
		this.primaryExchange = stockData.getString("primaryExchange");
		this.opens = stockData.getDouble("open");
		this.high = stockData.getDouble("high");
		this.low = stockData.getDouble("low");
		this.latestPrice = stockData.getDouble("latestPrice");
		this.closes = stockData.getDouble("close");
		this.changes = stockData.getDouble("change");
		this.changePercent = stockData.getDouble("changePercent");
		this.ytdChange = stockData.getDouble("ytdChange");
	}

	public double getChanges() {
		return changes;
	}

	public void setChanges(double change) {
		this.changes = change;
	}

	public Stock getStockOwner() {
		return stockOwner;
	}

	public void setStockOwner(Stock stockOwner) {
		this.stockOwner = stockOwner;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPrimaryExchange() {
		return primaryExchange;
	}

	public void setPrimaryExchange(String primaryExchange) {
		this.primaryExchange = primaryExchange;
	}

	public double getOpens() {
		return opens;
	}

	public void setOpens(double open) {
		this.opens = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getLatestPrice() {
		return latestPrice;
	}

	public void setLatestPrice(double latestPrice) {
		this.latestPrice = latestPrice;
	}

	public double getCloses() {
		return closes;
	}

	public void setCloses(double close) {
		this.closes = close;
	}

	public double getChangePercent() {
		return changePercent;
	}

	public void setChangePercent(double changePercent) {
		this.changePercent = changePercent;
	}

	public double getYtdChange() {
		return ytdChange;
	}

	public void setYtdChange(double ytdChange) {
		this.ytdChange = ytdChange;
	}

	public String getSymbolId() {
		return symbolId;
	}

	public void setSymbolId(String symbol) {
		this.symbolId = symbol;
	}
}
