package io.javabrains.StockQuote;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockQuoteService {
	@Autowired
	private StockQuoteRepositry stockQuoteRepository;

	public void addStockQuote(StockQuote newStockQuote) {
		stockQuoteRepository.save(newStockQuote);
	}

	public void addListStockQuotes(List<StockQuote> stockQouts) {
		stockQuoteRepository.save(stockQouts);

	}

	public StockQuote getStockQuote(String stockQuote) {
//		if (stockQuoteRepository.findBySymbol(stockQuote) == null)
//			return new StockQuote();
		return stockQuoteRepository.findBySymbolId(stockQuote);
	}

	public void addLisStockQuotes(List<StockQuote> stockQuote) {
		stockQuoteRepository.save(stockQuote);

	}

	public LinkedHashMap<Object, Object> mostChangedYesterday() {
		LinkedHashMap<Object, Object> res = new LinkedHashMap<>();
		List<StockQuote> stocks = stockQuoteRepository.findTop20ByOrderByChangePercentDesc();
		for (StockQuote stockQuote : stocks) {
			res.put(stockQuote.getSymbolId(), String.format("%.2f", (stockQuote.getChangePercent() * 100)) + "%");
		}
		return res;

	}
}
