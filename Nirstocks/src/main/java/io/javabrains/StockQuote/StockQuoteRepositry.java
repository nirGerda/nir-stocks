package io.javabrains.StockQuote;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface StockQuoteRepositry extends CrudRepository<StockQuote, String> {

	StockQuote findBySymbolId(String stockQuote);

	// get 20 stocks with highest CHANGE from last day
	List<StockQuote> findTop20ByOrderByChangePercentDesc();

}
