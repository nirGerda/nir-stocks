package io.javabrains;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XlsxHandler {

	private static DateFormat dateFormat = null;
	private static Date date = null;
	private static String[] columns = new String[2];
	private static Workbook workbook = null;
	private static Sheet sheet = null;
	private static Font headerFont = null;

	public static boolean PrintXlsxFile(LinkedHashMap<Object, Object> currCondStocks) throws IOException {
		/* init */
		initFile();
		/* headers */
		makeHeader();
		/* rows */
		makeRows(currCondStocks);
		/* style to cells */
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}
		/* write to xlsx file */
		FileOutputStream fileOut = new FileOutputStream("stock.xlsx");
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();
		return true;
	}

	private static void initFile() {
		dateFormat = new SimpleDateFormat("MM/dd");
		date = new Date();
		columns[0] = "up/stamp " + dateFormat.format(date);
		columns[1] = "worthBuy " + dateFormat.format(date);
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet("temp");
		headerFont = workbook.createFont();

	}

	private static void makeRows(LinkedHashMap<Object, Object> currCondStocks) {

		int rowNum = 1;
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		initCellStyle(cellStyle);

		HashMap<Object, Object> tempMap;
		for (Object s : currCondStocks.keySet()) {
			tempMap = (HashMap<Object, Object>) currCondStocks.get(s);
			Row row = sheet.createRow(rowNum);
			/* cell 0 - name of stock */
			Cell currCell0 = row.createCell(0);
			currCell0.setCellValue((String) s);
			currCell0.setCellStyle(cellStyle);
			/* cell 1 - cond */
			CellStyle cell1Style = sheet.getWorkbook().createCellStyle();
			cell1Style.cloneStyleFrom(cellStyle);
			Cell currCell1 = row.createCell(1);
			String currStatus = GraphStatus.makeStr((GraphStatus) tempMap.get("cond"));
			short color = ColorCell(currStatus);
			currCell1.setCellValue(currStatus);
			cell1Style.setFillForegroundColor(color);
			cell1Style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			currCell1.setCellStyle(cell1Style);
			/* cell 2 - change */
			Cell currCell2 = row.createCell(2);
			currCell2.setCellValue((String) tempMap.get("change"));
			currCell2.setCellStyle(cellStyle);
			rowNum++;
		}

	}

	private static void initCellStyle(CellStyle cellStyle) {
		cellStyle.setAlignment(HorizontalAlignment.CENTER);
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);

	}

	private static short ColorCell(String currStatus) {
		switch (currStatus) {
		case "up":
			return IndexedColors.LIME.getIndex();
		case "stampbad":
			return IndexedColors.RED1.getIndex();
		case "stampgood":
			return IndexedColors.ROYAL_BLUE.getIndex();
		}
		return IndexedColors.YELLOW.getIndex();
	}

	private static void makeHeader() {
		headerFont.setBold(true);
		headerFont.setColor(IndexedColors.RED.getIndex());
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		initCellStyle(headerCellStyle);
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

	}
}
