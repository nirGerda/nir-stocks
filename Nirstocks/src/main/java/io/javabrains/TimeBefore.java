package io.javabrains;

public enum TimeBefore {
	WEEK, TWOWEEKS, MONTH;
	public static TimeBefore makeEnum(String str) {
		str = str.toLowerCase();
		switch (str) {
		case "week":
			return WEEK;
		case "twoweeks":
			return TWOWEEKS;
		case "month":
			return MONTH;
		default:
			return WEEK;
		}
	}
}
