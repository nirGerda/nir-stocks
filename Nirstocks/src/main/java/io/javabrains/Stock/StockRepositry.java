package io.javabrains.Stock;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.javabrains.GraphStatus;

public interface StockRepositry extends CrudRepository<Stock, String> {

	Stock findBySymbolId(String id);

	List<Stock> findByCurrConditionOrderByWorthBuyDesc(GraphStatus currCondition);

	List<Stock> findByCurrConditionOrCurrConditionOrderByWorthBuyDesc(GraphStatus currCondition1,
			GraphStatus currCondition2);

	// get 5 stocks with highest CHANGE from last week
	List<Stock> findTop5ByOrderByChangeFromLastWeekDesc();

	// get 5 stocks with highest CHANGE from last two weeks
	List<Stock> findTop5ByOrderByChangeFromLastTwoWeeksDesc();

	// get 5 stocks with highest CHANGE from last month
	List<Stock> findTop5ByOrderByChangeFromLastMonthDesc();

	// get 5 stocks with highest UPS from last week
	List<Stock> findTop5ByOrderByUpsLastWeekDesc();

	// get 5 stocks with highest UPS from last two weeks
	List<Stock> findTop5ByOrderByUpsLastTwoWeeksDesc();

	// get 5 stocks with highest UPS from last month
	List<Stock> findTop5ByOrderByUpsLastMonthDesc();

	void deleteBySymbolId(String stock);

	void deleteBySymbolIdIn(List<String> stocks);

}
