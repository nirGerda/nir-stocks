package io.javabrains.Stock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.javabrains.GraphStatus;
import io.javabrains.TimeBefore;
import io.javabrains.StockQuote.StockQuote;
import io.javabrains.StockStatus.StockStatus;

@Entity
public class Stock {
	@Id
	private String symbolId;
	@OneToOne(mappedBy = "stockOwner", cascade = CascadeType.ALL)
	@JsonManagedReference
	private StockQuote stockQuote;
	private double worthBuy;
	private double changeFromLastWeek;
	private double changeFromLastTwoWeeks;
	private double changeFromLastMonth;
	private double changeFromLastMonthPrecent;
	private int upsLastWeek;
	private int upsLastTwoWeeks;
	private int upsLastMonth;
	private int downsLastWeek;
	private int downsLastTwoWeeks;
	private int downsLastMonth;
	private GraphStatus currCondition;
	@ElementCollection
	private List<GraphStatus> threeMonCond = new ArrayList<GraphStatus>();
	@OneToMany(mappedBy = "stockOwner", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<StockStatus> stockStatus = new ArrayList<>();

	public void setCurrCondition() {
		int mostUpdated = 0;
		List<GraphStatus> lastNineCond = threeMonCond.subList(mostUpdated, mostUpdated + 9);
		// increasing
		if ((threeMonCond.get(mostUpdated) == GraphStatus.UP) && (threeMonCond.get(mostUpdated + 1) == GraphStatus.UP)
				&& (threeMonCond.get(mostUpdated + 2) == GraphStatus.UP)) {
			this.currCondition = GraphStatus.UP;
			return;
		}
		// decreasing
		if ((threeMonCond.get(mostUpdated) == GraphStatus.DOWN)
				&& (threeMonCond.get(mostUpdated + 1) == GraphStatus.DOWN)
				&& (threeMonCond.get(mostUpdated + 2) == GraphStatus.DOWN)) {
			this.currCondition = GraphStatus.DOWN;
			return;
		}
		// stamp but with potential for increasing
		int indexOfFirstDown = lastNineCond.indexOf(GraphStatus.DOWN);
		int indexOfSecondDown = lastNineCond.lastIndexOf(GraphStatus.DOWN);
		// if have at least two downs
		if ((indexOfFirstDown != -1) && (indexOfSecondDown != -1) && indexOfFirstDown != indexOfSecondDown) {
			this.currCondition = GraphStatus.STAMPBAD;
			return;
		} // stamp but with potential for decreasing
		this.currCondition = GraphStatus.STAMPGOOD;

	}

	public List<GraphStatus> getThreeMonCond() {
		return threeMonCond;
	}

	public GraphStatus getCurrCondition() {
		return currCondition;
	}

	public void setCurrCondition(GraphStatus currCondition) {
		this.currCondition = currCondition;
	}

	public void setThreeMonCond(List<GraphStatus> threeMonCond) {
		this.threeMonCond = threeMonCond;
	}

	public Stock() {
	}

	public Stock(String id) {
		this.symbolId = id;
	}

	public Stock(StockQuote stockQuote, List<StockStatus> stockStatus) {
		this.symbolId = stockQuote.getSymbolId();
		this.stockQuote = stockQuote;
		this.stockStatus = stockStatus;

	}

	public StockStatus StockStatusBefore(TimeBefore age) {

		Date ageAgo = ageInDateFormat(age);
		int lastSt = this.getStockStatus().size() - 1;
		StockStatus currSt = null;
		for (int i = lastSt; i >= 0; i--) {
			currSt = this.getStockStatus().get(i);
			if (currSt.getDate().before(ageAgo)) {
				return this.getStockStatus().get(i);
			}
		}
		return this.getStockStatus().get(0);
	}

	private Date ageInDateFormat(TimeBefore age) {
		Calendar c = Calendar.getInstance();
		Date d;
		switch (age) {
		case WEEK:
			d = new Date(System.currentTimeMillis() - 7L * 24 * 3600 * 1000);
//			System.out.println("week: " + d);
			return d;
		case TWOWEEKS:
			d = new Date(System.currentTimeMillis() - 14L * 24 * 3600 * 1000);
//			System.out.println("two week: " + d);
			return d;
		case MONTH:
			c.add(Calendar.MONTH, -1);
			d = c.getTime();
//			System.out.println("month: " + d);
			return d;
		default:
			return null;
		}
	}

	public void updateChangesData(TimeBefore age) {
		StockStatus stockBeforeAge = this.StockStatusBefore(age);
		double valueAgeAgo = stockBeforeAge.getCloses();
		double changeFromAge = this.getStockQuote().getCloses() - valueAgeAgo;

		switch (age) {
		case WEEK:
			this.setChangeFromLastWeek(changeFromAge);
			break;
		case TWOWEEKS:
			this.setChangeFromLastTwoWeeks(changeFromAge);
			break;
		case MONTH:
			this.setChangeFromLastMonth(changeFromAge);
			this.setChangeFromLastMonthPrecent(changeFromAge / valueAgeAgo);
		default:
			;
		}
	}

	public void updateUpsDownsData(TimeBefore era) {
		List<StockStatus> relevants = this.statusInEra(era);
		/* true = Down, false = Up */
		switch (era) {
		case WEEK:
			this.setDownsLastWeek(CountUpDown(true, relevants));
			this.setUpsLastWeek(CountUpDown(false, relevants));
			break;
		case TWOWEEKS:
			this.setDownsLastTwoWeeks(CountUpDown(true, relevants));
			this.setUpsLastTwoWeeks(CountUpDown(false, relevants));
			break;
		case MONTH:
			this.setDownsLastMonth(CountUpDown(true, relevants));
			this.setUpsLastMonth(CountUpDown(false, relevants));
			break;
		default:
			this.setDownsLastWeek(CountUpDown(true, relevants));
			this.setUpsLastWeek(CountUpDown(false, relevants));
			break;

		}
	}

	private static int CountUpDown(boolean isDown, List<StockStatus> relevants) {
		int counter = 0;
		if (isDown) {
			for (StockStatus stat : relevants) {
				if (stat.getCloses() <= stat.getOpens())
					counter++;
			}
		} else {
			for (StockStatus stat : relevants) {
				if (stat.getCloses() > stat.getOpens())
					counter++;
			}
		}
		return counter;
	}

	public int getUpsLastWeek() {
		return upsLastWeek;
	}

	public void setUpsLastWeek(int upsLastWeek) {
		this.upsLastWeek = upsLastWeek;
	}

	public int getUpsLastTwoWeeks() {
		return upsLastTwoWeeks;
	}

	public void setUpsLastTwoWeeks(int upsLastTwoWeeks) {
		this.upsLastTwoWeeks = upsLastTwoWeeks;
	}

	public int getUpsLastMonth() {
		return upsLastMonth;
	}

	public void setUpsLastMonth(int upsLastMonth) {
		this.upsLastMonth = upsLastMonth;
	}

	public int getDownsLastWeek() {
		return downsLastWeek;
	}

	public void setDownsLastWeek(int downsLastWeek) {
		this.downsLastWeek = downsLastWeek;
	}

	public int getDownsLastTwoWeeks() {
		return downsLastTwoWeeks;
	}

	public void setDownsLastTwoWeeks(int downsLastTwoWeeks) {
		this.downsLastTwoWeeks = downsLastTwoWeeks;
	}

	public int getDownsLastMonth() {
		return downsLastMonth;
	}

	public void setDownsLastMonth(int downsLastMonth) {
		this.downsLastMonth = downsLastMonth;
	}

	public double getChangeFromLastTwoWeeks() {
		return changeFromLastTwoWeeks;
	}

	public void setChangeFromLastTwoWeeks(double changeFromLastTwoWeek) {
		this.changeFromLastTwoWeeks = changeFromLastTwoWeek;
	}

	public double getChangeFromLastMonth() {
		return changeFromLastMonth;
	}

	public void setChangeFromLastMonth(double changeFromLastMoth) {
		this.changeFromLastMonth = changeFromLastMoth;
	}

	public double getChangeFromLastWeek() {
		return changeFromLastWeek;
	}

	public void setChangeFromLastWeek(double changeFromLastWeek) {
		this.changeFromLastWeek = changeFromLastWeek;
	}

	public String getSymbolId() {
		return symbolId;
	}

	public void setSymbolId(String id) {
		this.symbolId = id;
	}

	public StockQuote getStockQuote() {
		return stockQuote;
	}

	public void setStockQuote(StockQuote stockQuote) {
		this.stockQuote = stockQuote;
	}

	public List<StockStatus> getStockStatus() {
		return stockStatus;
	}

	public void setStockStatus(List<StockStatus> stockStatus) {
		this.stockStatus = stockStatus;
		for (StockStatus curr : stockStatus) {
			curr.setStockOwner(this);
		}
	}

	public double getChangeFromLastMonthPrecent() {
		return changeFromLastMonthPrecent;
	}

	public double getWorthBuy() {
		return worthBuy;
	}

	public void setWorthBuy(double worthBuy) {
		this.worthBuy = worthBuy;
	}

	public void setWorthBuy() {

		this.worthBuy = 0.65 * (100 * this.getChangeFromLastMonthPrecent()) + 0.35 * this.getUpsLastMonth();
	}

	public void setChangeFromLastMonthPrecent(double changeFromLastMonthPrecent) {
		this.changeFromLastMonthPrecent = changeFromLastMonthPrecent;
	}

	public double ChangeByAge(TimeBefore age) {
		switch (age) {
		case WEEK:
			return this.getChangeFromLastWeek();
		case TWOWEEKS:
			return this.getChangeFromLastTwoWeeks();
		case MONTH:
			return this.getChangeFromLastMonth();
		default:
			return this.getChangeFromLastWeek();

		}
	}

	public List<StockStatus> statusInEra(TimeBefore age) {
		List<StockStatus> relStatus = new ArrayList<>();
		Date d = ageInDateFormat(age);
		for (StockStatus currStatus : this.getStockStatus()) {
			if (currStatus.getDate().after(d)) {
				relStatus.add(currStatus);
			}
		}
		return relStatus;
	}

	public double UpsMinusDownsByAge(TimeBefore era) {

		switch (era) {
		case WEEK:
			return this.getUpsLastWeek() - this.getDownsLastWeek();
		case TWOWEEKS:
			return this.getUpsLastTwoWeeks() - this.getDownsLastTwoWeeks();
		case MONTH:
			return this.getUpsLastMonth() - this.getDownsLastMonth();
		default:
			return this.getUpsLastWeek() - this.getDownsLastWeek();

		}
	}

	public void setGraphStatus() {
		int mostUpdate = this.stockStatus.size() - 1;
		while (mostUpdate - 6 >= 0) {
			// 6 days before updated
			StockStatus olderStockSt = this.stockStatus.get(mostUpdate - 6);
			// 3 days before updated
			StockStatus middleStockSt = this.stockStatus.get(mostUpdate - 3);
			// updated
			StockStatus youngerStockSt = this.stockStatus.get(mostUpdate);

			// check increasing
			if ((olderStockSt.getLow() < middleStockSt.getLow())
					&& (middleStockSt.getLow() < youngerStockSt.getLow())) {
				threeMonCond.add(GraphStatus.UP);
			} else {
				// check decreasing
				if ((olderStockSt.getHigh() > middleStockSt.getHigh())
						&& (middleStockSt.getHigh() > youngerStockSt.getHigh())) {
					threeMonCond.add(GraphStatus.DOWN);
				} else
					threeMonCond.add(GraphStatus.STAMP);
			}
			mostUpdate -= 3;
		}
		this.setCurrCondition();
	}

}
