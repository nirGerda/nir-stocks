package io.javabrains.Stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.javabrains.GraphStatus;
import io.javabrains.NirstocksApplication;
import io.javabrains.StockQuote.StockQuoteService;
import io.javabrains.StockStatus.StockStatus;
import io.javabrains.StockStatus.StockStatusService;

@Service
@Transactional
public class StockService {

	@Autowired
	private StockRepositry stockRepository;
	@Autowired
	private StockStatusService stockStatusService;
	@Autowired
	private StockQuoteService stockQuoteService;

	/* ****General Functions **** */

	public List<Stock> getAllStocks() {
		List<Stock> c = new ArrayList<>();
		stockRepository.findAll().forEach(c::add);
		return c;
	}

	public Stock getStockBySymbolName(String symbolName) throws Exception {
		Stock s = stockRepository.findBySymbolId(symbolName);
		CheckForErrors(s, "NotFoundException");
		return s;
	}

	public long getLastUpdateBySymbolName(String symbolName) throws Exception {
		Stock s = stockRepository.findBySymbolId(symbolName);
		CheckForErrors(s, "NotFoundException");
		List<StockStatus> statuses = s.getStockStatus();
//		System.out.println(symbolName + " " + statuses.size());
		return (statuses.get(statuses.size() - 1).getDate().getTime());

	}

	public List<Stock> getStockBySymbolName(List<String> symbolNameList) {
		List<Stock> res = new ArrayList<Stock>();
		symbolNameList.forEach(sym -> res.add(stockRepository.findBySymbolId(sym)));
		return res;
	}

	public void addStock(Stock newStock) {
		stockRepository.save(newStock);
	}

	public void UpdateStock(Stock newStock) {
		stockRepository.save(newStock);
	}

	public void updateListStocks(List<Stock> stocks) {
		stockRepository.save(stocks);

	}

	public void addListStocks(List<Stock> stocks) {
		stockRepository.save(stocks);

	}

	public void deleteListStocks(List<String> stocks) {
		stockRepository.deleteBySymbolIdIn(stocks);

	}

	public void deleteStockBySymbolId(String stock) {
		stockRepository.deleteBySymbolId(stock);

	}

	public void deleteAllStocks() {
		stockRepository.deleteAll();
	}

	/* **** Functions To Get/Set Information Of List Of stocks **** */

	public List<Stock> get5HighIncLastWeek() {
		return stockRepository.findTop5ByOrderByChangeFromLastWeekDesc();
	}

	public List<Stock> get5HighIncLastTwoWeeks() {
		return stockRepository.findTop5ByOrderByChangeFromLastTwoWeeksDesc();

	}

	public List<Stock> get5HighIncLastMonth() {
		return stockRepository.findTop5ByOrderByChangeFromLastMonthDesc();

	}

	public List<Stock> get5UpsDaysInLastWeek() {
		return stockRepository.findTop5ByOrderByUpsLastWeekDesc();
	}

	public List<Stock> get5UpsDaysInLastTwoWeeks() {
		return stockRepository.findTop5ByOrderByUpsLastTwoWeeksDesc();
	}

	public List<Stock> get5UpsDaysInLastMonth() {
		return stockRepository.findTop5ByOrderByUpsLastMonthDesc();
	}

	public List<String> getAllStocksSymbolId() {
		List<String> res = new ArrayList<>();
		List<Stock> stocks = getAllStocks();
		for (Stock s : stocks) {
			res.add(s.getSymbolId());
		}
		return res;
	}

	public int addListStocksBySymbols(List<String> stocksSymbols) throws Exception {
		return NirstocksApplication.getStocksData(this, stockQuoteService, stockStatusService, stocksSymbols);
	}

	public List<GraphStatus> getGraphStatusBySymbolName(String symbolId) throws Exception {
		return getStockBySymbolName(symbolId).getThreeMonCond();
	}

	public List<GraphStatus> getCondBySymbolName(List<String> symbolIdList) {
		List<GraphStatus> res = new ArrayList<GraphStatus>();
		List<Stock> stocks = getStockBySymbolName(symbolIdList);
		stocks.forEach(s -> res.add(s.getCurrCondition()));
		return res;
	}

	public LinkedHashMap<Object, Object> stocksChangeCondYesterday(List<String> stocksSymbols) {
		LinkedHashMap<Object, Object> res = new LinkedHashMap<Object, Object>();
		HashMap<Object, Object> tempMap = new HashMap<Object, Object>();
		stocksSymbols.forEach(elem -> elem.toUpperCase());
		Stock currSt;
		List<Stock> stocks = getStockBySymbolName(stocksSymbols);
		// stocks.sort((s1, s2) -> Double.compare(s1.getWorthBuy(), s2.getWorthBuy()));
		for (int i = 0; i < stocks.size(); i++) {
			tempMap = new HashMap<Object, Object>();
			currSt = stocks.get(i);
			tempMap.put("closes", currSt.getStockQuote().getCloses());
			tempMap.put("cond", currSt.getCurrCondition());
			tempMap.put("change", String.format("%.2f", (currSt.getStockQuote().getChangePercent() * 100)) + "%");
//			tempMap.put("change in last month precent", currSt.getChangeFromLastMonthPrecent());
//			tempMap.put("change from beginning of the year", currSt.getStockQuote().getYtdChange());
//			tempMap.put("worth to buy", currSt.getWorthBuy());
			res.put(currSt.getSymbolId(), tempMap);

		}
		return res;
	}

	public List<Stock> getAllUpsCondsStocks(GraphStatus cond) {
		List<Stock> c = new ArrayList<>();
		stockRepository.findByCurrConditionOrCurrConditionOrderByWorthBuyDesc(cond, GraphStatus.STAMPGOOD)
				.forEach(c::add);
		return c;
	}

	public List<Double> value3DaysAgo(List<String> symbolName) throws Exception {
		List<Stock> stocks = getStockBySymbolName(symbolName);
		StockStatus lastSt, threeDaysPrevSt;
		double diffLastToThreeDays;
		List<Double> res = new ArrayList<>();
		for (Stock s : stocks) {
			lastSt = s.getStockStatus().get(s.getStockStatus().size() - 1);
			threeDaysPrevSt = s.getStockStatus().get(s.getStockStatus().size() - 4);
			diffLastToThreeDays = lastSt.getCloses() - threeDaysPrevSt.getCloses();
			res.add(diffLastToThreeDays / threeDaysPrevSt.getCloses());
		}
		return res;
	}

	/* Error Handling */

	private void CheckForErrors(Stock s, String str) throws Exception {
		switch (str) {
		case "NotFoundException":
			if (s == null)
				throw new NotFoundException("ERROR: this symbol does not exist");
		default:
			break;
		}
	}

}
