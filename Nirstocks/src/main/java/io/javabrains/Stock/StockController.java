package io.javabrains.Stock;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.GraphStatus;
import io.javabrains.TimeBefore;
import io.javabrains.XlsxHandler;
import io.javabrains.StockQuote.StockQuote;
import io.javabrains.StockQuote.StockQuoteService;
import io.javabrains.StockStatus.StockStatus;
import io.javabrains.StockStatus.StockStatusService;

@RestController
public class StockController {
	@Autowired
	private StockService stockService;
	@Autowired
	private StockQuoteService stockQuoteService;
	@Autowired
	private StockStatusService stockStatusService;

	/* ****for heroku *****/
	@RequestMapping("/")
	public String general() {
		return "Welcome To Nir Gerda Server!";
	}

	/* ****General Functions **** */
	// AllStocks
	@RequestMapping("/AllStocks")
	public List<Stock> getAllStocks() {
		List<Stock> res = stockService.getAllStocks();
		return res;
	}

	// addStocks
	@RequestMapping(path = "/addStocks", method = RequestMethod.POST)
	public String addStocks(@RequestBody List<String> stocksSymbols) throws Exception {
		int numOfStocks = stockService.addListStocksBySymbols(stocksSymbols);
		return numOfStocks + " stocks were added / uploaded";
	}

	// {symbolName}
	@RequestMapping(path = "/{symbolName}", method = RequestMethod.GET)
	public Stock getStockBySymbolName(@PathVariable String symbolName) throws Exception {
		Stock s = stockService.getStockBySymbolName(symbolName.toUpperCase());
		return s;
	}

	// {symbolName}
	@RequestMapping(path = "/getStocks", method = RequestMethod.POST)
	public List<Stock> getStocksBySymbolName(@RequestBody List<String> stocksSymbols) throws Exception {
		List<Stock> stocks = stockService.getStockBySymbolName(stocksSymbols);
		return stocks;
	}

	// {symbolName}
	@RequestMapping(path = "/{symbolName}", method = RequestMethod.DELETE)
	public String deleteStockBySymbolName(@PathVariable String symbolName) {
		stockService.deleteStockBySymbolId(symbolName);
		return symbolName + " was deleted";
	}

	// deleteStocks
	@RequestMapping(path = "/deleteStocks", method = RequestMethod.DELETE)
	public String deleteListStocks(@RequestBody List<String> stocksToDelete) {
		stockService.deleteListStocks(stocksToDelete);
		return stocksToDelete.size() + " stocks were deleted";

	}

	// deleteAllStocks
	@RequestMapping(path = "/deleteAllStocks", method = RequestMethod.DELETE)
	public String deleteAllStocks() {
		stockService.deleteAllStocks();
		return "All stocks were deleted";

	}

	/* **** Functions To Get Fields Information Of Specific stock **** */
	/// quote/{symbolName}
	@RequestMapping("/quote/{symbolName}")
	public StockQuote getStockQuote(@PathVariable String symbolName) {
		return stockQuoteService.getStockQuote(symbolName.toUpperCase());
	}

	// history/{symbolName}
	@RequestMapping("/history/{symbolName}")
	public List<StockStatus> getStockStatus(@PathVariable String symbolName) throws Exception {
		Stock stock = stockService.getStockBySymbolName(symbolName);
		return stockStatusService.getStockStatus(stock);
	}

	// graphStatus/{symbolName}
	@RequestMapping("graphStatus/{symbolName}")
	public HashMap<Object, Object> getGraphStatusBySymbolName(@PathVariable String symbolName) throws Exception {
		HashMap<Object, Object> res = new HashMap<Object, Object>();
		res.put(symbolName, stockService.getGraphStatusBySymbolName(symbolName.toUpperCase()));
		return res;
	}

	// currCond/{symbolName}
	@RequestMapping("currCond/{symbolName}")
	public HashMap<Object, Object> getCurrCondBySymbolName(@PathVariable String symbolName) {
		HashMap<Object, Object> res = new HashMap<Object, Object>();
		res.put(symbolName, stockService.getCondBySymbolName(Arrays.asList(symbolName.toUpperCase())).get(0));
		return res;
	}

	// currCond
	@RequestMapping(path = "/currCond", method = RequestMethod.POST)
	public LinkedHashMap<Object, Object> getCurrCondList(@RequestBody List<String> stocksSymbols) throws IOException {
		LinkedHashMap<Object, Object> res = new LinkedHashMap<Object, Object>();
		stocksSymbols.forEach(elem -> elem.toUpperCase());
		List<GraphStatus> stocksCond = stockService.getCondBySymbolName(stocksSymbols);
		for (int i = 0; i < stocksSymbols.size(); i++) {
			HashMap<Object, Object> curr = new HashMap<Object, Object>();
			res.put(stocksSymbols.get(i), stocksCond.get(i));
		}
		return res;
	}

	// stocksChangeCondYesterday
	@RequestMapping(path = "/stocksChangeCondYesterday", method = RequestMethod.POST)
	public LinkedHashMap<Object, Object> stocksChangeCondYesterday(@RequestBody List<String> stocksSymbols) {
		return stockService.stocksChangeCondYesterday(stocksSymbols);
	}

	@RequestMapping(path = "/PrintXlsx", method = RequestMethod.POST)
	public boolean printXlsx(@RequestBody List<String> stocksSymbols) {
		try {
			LinkedHashMap<Object, Object> currCondStocks = stocksChangeCondYesterday(stocksSymbols);
			XlsxHandler.PrintXlsxFile(currCondStocks);
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
			return false;
		}
		return true;
	}

	/* **** Functions To Get Information Of List Of stocks **** */

	// AllStocksUpCond
	@RequestMapping("/AllStocksUpCond")
	public LinkedHashMap<Object, Object> getAllUpsCondsStocks() {
		LinkedHashMap<Object, Object> res = new LinkedHashMap<Object, Object>();
		Stock currSt;
		LinkedHashMap<Object, Object> tempMap = new LinkedHashMap<Object, Object>();
		List<Stock> stocks = stockService.getAllUpsCondsStocks(GraphStatus.UP);
		int counterIndx = 1;
		for (int i = 0; i < stocks.size(); i++) {
			currSt = stocks.get(i);
			// number of ups in last month is bigger at least by two of downs last month
			if (currSt.getUpsLastMonth() < currSt.getDownsLastMonth() + 2)
				continue;
			StockStatus lastSt = currSt.getStockStatus().get(currSt.getStockStatus().size() - 1);
			StockStatus threeDaysPrevSt = currSt.getStockStatus().get(currSt.getStockStatus().size() - 4);
			double diffLastToThreeDays = lastSt.getCloses() - threeDaysPrevSt.getCloses();
			double diffLastToThreeDaysPrec = diffLastToThreeDays / threeDaysPrevSt.getCloses();
			// only if the stock really increased
			if (diffLastToThreeDays < 0.0)
				continue;
			tempMap = new LinkedHashMap<Object, Object>();
			tempMap.put("index", counterIndx);
			tempMap.put("condition ", currSt.getCurrCondition());
			tempMap.put("worth to buy", currSt.getWorthBuy());
			tempMap.put("closes", currSt.getStockQuote().getCloses());
			tempMap.put("diffrence last 3 days", diffLastToThreeDays);
			tempMap.put("diffrence last 3 days precent", String.format("%.2f", (diffLastToThreeDaysPrec * 100)) + "%");
			tempMap.put("change last month precent",
					String.format("%.2f", (currSt.getChangeFromLastMonthPrecent() * 100)) + "%");
			tempMap.put("change  beginning of the year",
					String.format("%.2f", (currSt.getStockQuote().getYtdChange() * 100)) + "%");
			res.put(currSt.getSymbolId(), tempMap);
			counterIndx++;
		}
		return res;
	}

	@RequestMapping("/mostChangedYesterday")
	public LinkedHashMap<Object, Object> mostChangedYesterday() {
		return stockQuoteService.mostChangedYesterday();
	}

	// stock's change in last 3 days
	@RequestMapping(path = "/value3DaysAgo", method = RequestMethod.POST)
	public HashMap<Object, Object> value3DaysAgo(@RequestBody List<String> stocksSymbols) throws Exception {

		HashMap<Object, Object> res = new HashMap<Object, Object>();
		List<Double> data = stockService.value3DaysAgo(stocksSymbols);
		for (int i = 0; i < stocksSymbols.size(); i++) {
			res.put(stocksSymbols.get(i), String.format("%.2f", (data.get(i) * 100)) + "%");
		}
		return res;
	}

	// highestUps/{era}
	@RequestMapping(path = "/highestUps/{era}", method = RequestMethod.GET)
	public HashMap<Object, Object> numberOfUpsInEra(@PathVariable("era") String strEra) {
		HashMap<Object, Object> res = new HashMap<Object, Object>();
		TimeBefore era = TimeBefore.makeEnum(strEra);
		List<Stock> best5;
		switch (era) {
		case WEEK:
			best5 = stockService.get5UpsDaysInLastWeek();
		case TWOWEEKS:
			best5 = stockService.get5UpsDaysInLastTwoWeeks();
		case MONTH:
			best5 = stockService.get5UpsDaysInLastMonth();
		default:
			best5 = stockService.get5UpsDaysInLastWeek();
		}
		for (Stock s : best5) {
			res.put(s.getStockQuote().getCompanyName(), s.UpsMinusDownsByAge(era));
		}
		return res;

	}

	// get 5 company names, that increase the most in the last week
	// highestIncWeek
	@RequestMapping(path = "/highestIncWeek", method = RequestMethod.GET)
	public HashMap<Object, Object> highestStockLastWeek() {
		return highestStockLastAge(TimeBefore.WEEK);
	}

	// highestIncTwoWeeks
	@RequestMapping("/highestIncTwoWeeks")
	public HashMap<Object, Object> highestStockLastTwoWeeks() {
		return highestStockLastAge(TimeBefore.TWOWEEKS);
	}

	// highestIncMonth
	@RequestMapping("/highestIncMonth")
	public HashMap<Object, Object> highestStockLastMonth() {
		return highestStockLastAge(TimeBefore.MONTH);
	}

	private HashMap<Object, Object> highestStockLastAge(TimeBefore age) {
		HashMap<Object, Object> res = new HashMap<Object, Object>();
		List<Stock> best5 = BestFiveByAge(age);
		for (Stock s : best5) {
			res.put(s.getStockQuote().getCompanyName(), s.ChangeByAge(age));
		}
		return res;
	}

	private List<Stock> BestFiveByAge(TimeBefore age) {
		switch (age) {
		case WEEK:
			return stockService.get5HighIncLastWeek();
		case TWOWEEKS:
			return stockService.get5HighIncLastTwoWeeks();
		case MONTH:
			return stockService.get5HighIncLastMonth();
		default:
			return stockService.get5HighIncLastWeek();
		}
	}

}
