package io.javabrains;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import io.javabrains.Stock.Stock;
import io.javabrains.Stock.StockService;
import io.javabrains.StockQuote.StockQuote;
import io.javabrains.StockQuote.StockQuoteService;
import io.javabrains.StockStatus.StockStatus;
import io.javabrains.StockStatus.StockStatusService;

@SpringBootApplication
@EnableScheduling
public class NirstocksApplication {

	public static void main(String[] args) {
		SpringApplication.run(NirstocksApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(StockService stockService, StockQuoteService stockQuoteService,
			StockStatusService stockStatusService) {
		return args -> {
			getStocksData(stockService, stockQuoteService, stockStatusService);
		};
	}

	public static int getStocksData(StockService stockService, StockQuoteService stockQuoteService,
			StockStatusService stockStatusService) throws Exception {
		return getStocksData(stockService, stockQuoteService, stockStatusService, Collections.<String>emptyList());

	}

	public static int getStocksData(StockService stockService, StockQuoteService stockQuoteService,
			StockStatusService stockStatusService, List<String> stocksSymbols) throws Exception {
		long start = System.currentTimeMillis();
		List<String> errorStocks = new ArrayList<>();// for Stocks with errors
		Stock currStock;
		StockQuote currStockQuote = null;
		StockStatus currStockStatus = null;
		List<StockQuote> quotesList = new ArrayList<>();
		List<StockStatus> StatusesList = new ArrayList<>();
		List<StockStatus> stockStatusList = new ArrayList<>();
		List<String> companiesSymbols = stocksSymbols.isEmpty() ? takeStocksName(stockService) : stocksSymbols;
		int forMassage = companiesSymbols.size();
		while (!companiesSymbols.isEmpty()) { // for chunks of 10 stocks at a time
			System.out.println("size of companies left : " + companiesSymbols.size());
			List<String> currChunk = companiesSymbols.size() >= 10 ? companiesSymbols.subList(0, 10) : companiesSymbols;
			String companies = currChunk.stream().collect(Collectors.joining(","));
			companiesSymbols.removeAll(currChunk);
			String uri = "https://api.iextrading.com/1.0/stock/market/batch?symbols=" + companies
					+ "&types=quote,chart&range=3m";
			RestTemplate restTemplate = new RestTemplate();
			// get the Object sorted by Company (String)
			String JsonStocksString = restTemplate.getForObject(uri, String.class);
			// convert to JSONObject
			JSONObject jsonObject = new JSONObject(JsonStocksString.trim());
			// iterate over the companies
			for (Object company : jsonObject.keySet()) {
				try {
					String compStr = (String) company;
					Object keyvalue = jsonObject.get(compStr);
					// initial new stock
					currStock = new Stock("just for now");
					if (keyvalue instanceof JSONObject) {
						if (CheckIfNotNeedUpdate(keyvalue, stockService))
							continue;
						Object stockQuote = ((JSONObject) keyvalue).get("quote");
						// create new stockQuote
						if (stockQuote instanceof JSONObject) {
							// init the new ID for stock,
							String stockSymbol = ((JSONObject) stockQuote).getString("symbol");
							stockStatusService.deleteByStockSymbol(stockSymbol);
							currStock.setSymbolId(stockSymbol);
							currStockQuote = new StockQuote((JSONObject) stockQuote);
							// set the quote owner
							currStockQuote.setStockOwner(currStock);
							// add to list - the list is insert to table at the end
							quotesList.add(currStockQuote);
						}
						// init list status history of current stock
						stockStatusList.clear();
						JSONArray stockCharts = ((JSONObject) keyvalue).getJSONArray("chart");
						for (int i = 0; i < stockCharts.length(); i++) {
							currStockStatus = new StockStatus(stockCharts.getJSONObject(i));
							currStockStatus.setStockOwner(currStock);
							stockStatusList.add(currStockStatus);
							// add to list - the list is insert to table at the end
							StatusesList.add(currStockStatus);
						}
						// set the quote and history status of current stock
						currStock.setStockQuote(currStockQuote);
						currStock.setStockStatus(stockStatusList);
						CalcChanges(currStock);
						// add to list - the list is insert to table at the end
					}
					stockService.addStock(currStock);
				} catch (Exception ex) {
					System.out.println("error: " + ex.getMessage());
					errorStocks.add((String) company);
				}
			}

			/*
			 * need for JPA use, but no to MYSQL
			 * stockQuoteService.addLisStockQuotes(quotesList);
			 * stockStatusService.addLisStockstStatus(StatusesList);
			 */
		}
		// error is
		long elapsedTimeMillis = System.currentTimeMillis() - start;
		System.out.println("DB is Updated in " + elapsedTimeMillis / 1000F + " sec");
		if (!errorStocks.isEmpty())// ERRORS CASE
			throw new Exception("ERROR : problems with the following stocks:\n" + String.join(", ", errorStocks)
					+ "\n\nPay Attention- all other " + (forMassage - errorStocks.size())
					+ " stocks were added Successfully ");
		return forMassage;

	}

	private static boolean CheckIfNotNeedUpdate(Object keyvalue, StockService stockService) throws Exception {
		Object stockQuote = ((JSONObject) keyvalue).get("quote");
		String s = ((JSONObject) stockQuote).getString("symbol");
		JSONArray stockCharts = ((JSONObject) keyvalue).getJSONArray("chart");
		StockStatus lastStatus = new StockStatus(stockCharts.getJSONObject(stockCharts.length() - 1));
		long fromDBDate, fromNewDataDate = lastStatus.getDate().getTime();
		try {
			fromDBDate = stockService.getLastUpdateBySymbolName(s);
		} catch (Exception ex) {
			return false;
		}

		return fromNewDataDate == fromDBDate;
	}

	private static List<String> takeStocksName(StockService stockService) {
		List<String> result = Arrays.asList("fb", "wix", "googl", "aapl", "momo", "amzn", "dvmt");
		try {
			result = stockService.getAllStocksSymbolId();
		} catch (Exception ex) {
			result = Arrays.asList("aapl", "wix", "googl", "fb", "momo", "amzn", "dvmt");
			System.out.print("ooooppsss : ");
		}
		return result;
	}

	private static void CalcChanges(Stock s) {
		for (TimeBefore age : TimeBefore.values()) {
			// stock value changes
			s.updateChangesData(age);
			// number of ups and downs
			s.updateUpsDownsData(age);
			// stock - increase or decrease
			s.setGraphStatus();
			// set worth to buy the stock
			s.setWorthBuy();
		}
	}
}
