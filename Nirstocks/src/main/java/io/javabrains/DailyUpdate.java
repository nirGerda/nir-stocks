package io.javabrains;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import io.javabrains.Stock.StockService;
import io.javabrains.StockQuote.StockQuoteService;
import io.javabrains.StockStatus.StockStatusService;

@Component
public class DailyUpdate {
	private int counterUpdated = 0;
	private static final Logger logger = LoggerFactory.getLogger(DailyUpdate.class);
	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	@Autowired
	StockService stockService;
	@Autowired
	StockQuoteService stockQuoteService;
	@Autowired
	StockStatusService stockStatusService;

//	update every day in 23:00:00
	@Scheduled(cron = "0 0 23 * * ?")
	public void scheduleTaskWithFixedRate() throws Exception {
		logger.info("Execution update Time - {}", dateTimeFormatter.format(LocalDateTime.now()));
		counterUpdated++;
		logger.info("Counter-loop:" + counterUpdated);
		NirstocksApplication.getStocksData(stockService, stockQuoteService, stockStatusService);

	}
}
