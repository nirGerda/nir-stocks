package io.javabrains;

public enum GraphStatus {
	DOWN, UP, STAMP, STAMPGOOD, STAMPBAD;
	public static GraphStatus makeEnum(String str) {
		str = str.toLowerCase();
		switch (str) {
		case "down":
			return DOWN;
		case "up":
			return UP;
		case "stamp":
			return STAMP;
		case "stampgood":
			return STAMPGOOD;
		case "stampbad":
			return STAMPBAD;
		default:
			return DOWN;
		}
	}

	public static String makeStr(GraphStatus gs) {
		switch (gs) {
		case DOWN:
			return "down";
		case UP:
			return "up";
		case STAMP:
			return "stamp";
		case STAMPGOOD:
			return "stampgood";
		case STAMPBAD:
			return "stampbad";
		default:
			return "down";
		}
	}
}
